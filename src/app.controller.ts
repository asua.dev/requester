import { Controller, Get, Post, Put, Delete, Patch, Options, Head, Headers, Body, Req, Query } from "@nestjs/common";

import { AppService } from './app.service';
import { FormDataRequest } from "nestjs-form-data";

@Controller()
export class AppController {

  constructor(private readonly appService: AppService) {}

  @Get('/*')
  get(
    @Req() request: Request,
    @Headers() headers: any,
    @Query() query: any,
  ): string {
    console.log('=============== Request ====================');
    console.log('METHOD: ' + request.method);
    console.log('PATH: ' + request.url);
    console.log('=============== Headers');
    console.log(headers);
    console.log('=============== query');
    console.log(query);

    let out = '';
    out += '<br /> ================= REQUEST ============';
    out += '<br /> METHOD: ' + request.method;
    out += '<br /> PATH: ' + request.url;
    out += '<br /> =============== Headers';
    out += '<br /><pre>' + JSON.stringify(headers, null, 4) + '<pre />';
    out += '<br /> =============== Query';
    out += '<br /><pre>' + JSON.stringify(query, null, 4) + '<pre />';

    return out;
  }

  @Post('/*')
  @FormDataRequest()
  post(
    @Req() request: Request,
    @Headers() headers: any,
    @Body() body: any,
    @Query() query: any,
  ): string {
    console.log('=============== Request ====================');
    console.log('METHOD: ' + request.method);
    console.log('PATH: ' + request.url);
    console.log('=============== Headers');
    console.log(headers);
    console.log('=============== query');
    console.log(query);
    console.log('=============== Body');
    console.log(body);
    return 'look to terminal';
  }

  /*
  @Put()
  put(@Headers() headers, @Body() body: any): string {
    return this.appService.getHello();
  }

  @Patch()
  patch(@Headers() headers, @Body() body: any): string {
    return this.appService.getHello();
  }

  @Delete()
  delete(@Headers() headers, @Body() body: any): string {
    return this.appService.getHello();
  }

  @Options()
  options(@Headers() headers, @Body() body: any): string {
    return this.appService.getHello();
  }

  @Head()
  head(@Headers() headers, @Body() body: any): string {
    return this.appService.getHello();
  } */

}
